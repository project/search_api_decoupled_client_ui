# Search API decoupled / ui

This is a UI component library for the Search API decoupled client.

It's built with CSS and Typescript.

The components expect to find a specific id attribute on divs, then they will use those divs as entrypoints, getting the rest of attributes as data and rendering the component. Those components are expected to throw events in order to interact with the client.

The client `@search-api-decoupled/client` will provide the data and the entrypoints, it will follow specific interfaces for each type of element and widget. All the components should follow those interfaces in order to work with the client.

These components will be consumed as libraries in the `search api decoupled ui` drupal module in order to provide ui elements to the client.

## Local Setup

1. Cloning the monorepo:
   - `git clone git@git.drupal.org:project/search_api_decoupled_client_ui.git`
2. Install app dependencies:
   - `nvm use && npm i`
3. Development:
   - use `npm run dev` to run the dev env with the elements introduced in a simple html file
   - you can adapt the `index.html` in order to add or remove attributes to the mocked elements
   - it's recommended to actually test changes on this package using the client dev env
     - check the [client repository](https://git.drupalcode.org/project/search_api_decoupled_client)
     - clone it next to this repository
     - on `package.json` change the dependency to use the local version of this package using `file:../search_api_decoupled_client_ui`
