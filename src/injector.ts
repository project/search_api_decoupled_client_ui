const observeComponentsByType = (
  typeName: string,
  CompDef: (element: HTMLDivElement) => void,
) => {
  setInterval(function () {
    handleInjections(typeName, CompDef, document);
  }, 1);
};

const handleInjections = (
  typeName: string,
  ComponentDefinition: (element: HTMLDivElement) => void,
  base: HTMLElement | Document,
) => {
  const results: NodeListOf<HTMLDivElement> = base.querySelectorAll(
    `[data-element-type^="${typeName}"]`,
  );

  results.forEach((c) => {
    if (!(c.dataset && "elementTypeProcessed" in c.dataset)) {
      ComponentDefinition(c);
      c.dataset.elementTypeProcessed = "true";
      // Observe for changes.
      const observer = new MutationObserver(() => {
        ComponentDefinition(c);
      });
      observer.observe(c, { attributes: true });
    }
  });
};

export default observeComponentsByType;
