import "./chip.css";
import observeComponentsByType from "../../injector.ts";

// * id = "facet-${widget}"
observeComponentsByType("facet-chip", Chip);
observeComponentsByType("facet-checkbox", Chip);

/*
 * ATTRIBUTES PASSED:
 ? facet: string
 ? key: string
 ? active: boolean
 ? count: number
 ? show-count: boolean
 ? label: string
 ? children: ...[];

 * EVENT LISTENERS: (ensure your component dispatches with bubbles: true)
 ? facet-change: {detail: { key, active, reset}}
 */

export function Chip(element: HTMLDivElement) {
  const key = element.getAttribute("facet-key");
  const active = element.hasAttribute("active") ?? false;
  const count = element.getAttribute("count") ?? 0;
  const showCount = element.hasAttribute("show-count") ?? false;
  const label = element.getAttribute("label");

  const onChipClick = () => {
    element.dispatchEvent(
      new CustomEvent("facet-change", {
        bubbles: true,
        detail: {
          key: key,
          active: !active,
          reset: false,
        },
      }),
    );
  };

  element.innerHTML = `
    <span
      class="chip"
      role="button"
      tabindex="0"
    >
      <span class="label">${label}</span>
      ${showCount ? `<span class="count">${count}</span>` : ""}
    </span>
  `;

  element.addEventListener("click", onChipClick);
}
