import "./pager.css";
import back from "../../assets/back.svg";
import forward from "../../assets/forward.svg";
import observeComponentsByType from "../../injector.ts";

// * id = "pager-${widget}"
observeComponentsByType("pager-full", FullPager);

let backFullPath = back
let forwardFullPath = forward

// For prod we need to calculate the url in realtime to
// allow this component live at any unknown dir.
if(import.meta.env.PROD){
  // @ts-ignore
  backFullPath = import.meta.resolve("../assets/svg/back.svg")
  // @ts-ignore
  forwardFullPath = import.meta.resolve("../assets/svg/forward.svg")
}

/*
 * ATTRIBUTES PASSED:
 ? current-page: number
 ? pages: number

  * EVENT LISTENERS: (ensure your component dispatches with bubbles: true):
 ? page-change: {detail: number}
*/

export function FullPager(element: HTMLDivElement) {
  const currentPage = Number(element.getAttribute("current-page"));
  const pages = Number(element.getAttribute("pages") ?? "0");

  const onPageClick = (page: number) => {
    element.dispatchEvent(
      new CustomEvent("page-change", {
        bubbles: true,
        detail: page,
      }),
    );
  };

  const onPrev = () => {
    onPageClick(currentPage - 1);
  };

  const onNext = () => {
    onPageClick(currentPage + 1);
  };

  if (pages < 1) {
    return;
  } else {
    element.innerHTML = `
      <div class="pager">
        <button 
          class="prev" 
          ${currentPage === 0 ? "disabled" : ""}
        >
          <img src="${backFullPath}" />
        </button>
        ${Array.from({ length: pages }, (_, i) => i + 1)
          .map(
            (page) => `
            <button
              class="${currentPage === page - 1 ? "page active" : "page"}"
            >
              ${page}
            </button>
          `,
          )
          .join("")}
        <button
          class="next"
          ${!pages || currentPage === pages - 1 ? "disabled" : ""}
        >
          <img src="${forwardFullPath}" />
        </button>
      </div>
    `;

    element.querySelector("button.prev")?.addEventListener("click", onPrev);
    element.querySelector("button.next")?.addEventListener("click", onNext);
    element.querySelectorAll("button.page")?.forEach((page, index) => {
      page.addEventListener("click", () => {
        onPageClick(index);
      });
    });
  }
}
