import "./input.css";
import observeComponentsByType from "../../injector";
import close from "../../assets/close.svg";
import search from "../../assets/search.svg";

// * data-element-type = "{element_type}-{widget}"

observeComponentsByType("search_input-default", Input);

let closeFullPath = close
let searchFullPath = search

// For prod we need to calculate the url in realtime to
// allow this component live at any unknown dir.
if(import.meta.env.PROD){
  // @ts-ignore
  closeFullPath = import.meta.resolve("../assets/svg/close.svg")
  // @ts-ignore
  searchFullPath = import.meta.resolve("../assets/svg/search.svg")
}

/*
 * ATTRIBUTES PASSED:
 ? value: string
 ? placeholder: string

 * EVENT LISTENERS: 
 * (ensure your component dispatches with bubbles: true):
 ? input-change: {detail: { value }}
 ? input-reset: {}
 ? input-submit: {}
*/

export function Input(element: HTMLDivElement) {
  const value = element.getAttribute("value") ?? "";
  const placeholder = element.getAttribute("placeholder");

  const onInputChange = (e: Event) => {
    const newValue = (e.target as HTMLInputElement).value;
    if (newValue === "") {
      onReset();
    }
    element.dispatchEvent(
      new CustomEvent("input-change", {
        bubbles: true,
        detail: {
          value: (e.target as HTMLInputElement).value,
        },
      }),
    );
  };
  const onReset = () => {
    element.dispatchEvent(
      new CustomEvent("input-reset", {
        bubbles: true,
        detail: {},
      }),
    );
  };
  const onSubmit = () => {
    element.dispatchEvent(
      new CustomEvent("input-submit", {
        bubbles: true,
        detail: {},
      }),
    );
  };
  const onKeyUp = (e: KeyboardEvent) => {
    if (e.key === "Enter") {
      onSubmit();
    }
  };

  element.innerHTML = `
    <input
      type="text"
      placeholder="${placeholder}"
      ${value ? `value="${value}"` : ""}
    />
    <img src="${searchFullPath}" class="search" />
    ${value ? `<img src="${closeFullPath}" class="close" />` : ""}
  `;

  const input = element.querySelector("input") as HTMLInputElement;
  const submit = element.querySelector(".search");
  const reset = element.querySelector(".close");

  input.addEventListener("input", onInputChange);
  input.addEventListener("keyup", onKeyUp);
  submit?.addEventListener("click", onSubmit);
  reset?.addEventListener("click", onReset);
}
