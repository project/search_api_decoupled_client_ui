import "./summary.css";
import observeComponentsByType from "../../injector.ts";

// * id = "summary"
observeComponentsByType("summary", Summary);

/*
 * ATTRIBUTES PASSED:
 ? time-taken: number (in ms)
 ? number-of-results: number
 */

export function Summary(element: HTMLDivElement) {
  const timeTaken = Number(element.getAttribute("time-taken") ?? 0);
  const nOfResults = element.getAttribute("number-of-results") ?? 0;

  element.innerHTML = `
    <div class="summary">
      <div>Found ${nOfResults} results;</div>
      <div>In ${(timeTaken / 1000).toPrecision(8)}s</div>
    </div>
  `;
}
