import "./result.css";
import observeComponentsByType from "../../injector";

// * id = "result-${widget}"
observeComponentsByType("search_result_custom", Result);

/*
 * ATTRIBUTES PASSED:
 ? category: string
 ? date: string
 ? title: string
 ? snippet: string
 ? excerpt: string
 ? text: string
 ? url: string
 ? preview_image: string
 */

export function Result(element: HTMLDivElement) {
  const category = element.getAttribute("category");
  const date = element.getAttribute("date");
  const title = element.getAttribute("title");
  const text =
    element.querySelector("[slot='text']")?.innerHTML ??
    element.querySelector(".result__excerpt")?.innerHTML;

  const url = element.getAttribute("url");
  const previewImage = element.getAttribute("preview_image");

  const onOpen = () => {
    if (url) window.open(url, "_blank");
  };

  element.innerHTML = `
    <div class="container">
      <div class="result">
        <div class="result__content">
          <div class="result__info">
            ${
              category
                ? `<span class="result__category">${category}</span>`
                : ""
            }
            ${date ? `<span class="result__date">${date}</span>` : ""}
          </div>
          <div class="result__title">${title}</div>
          <div class="result__excerpt">${text}</div>
        </div>
        <div class="result__image">
          <img src="${previewImage}" />
        </div>
      </div>
    </div>
  `;

  element.onclick = onOpen;
}
