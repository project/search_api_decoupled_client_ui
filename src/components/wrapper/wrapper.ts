import "./wrapper.css";
import observeComponentsByType from "../../injector.ts";

// * id = "wrapper-{element}-{widget}"
observeComponentsByType("wrapper-", Wrapper);

/*
 * ATTRIBUTES PASSED:
 ? loading: boolean
 ? loading-text: string
 ? columns: number
 ? orientation: string
 ? mode: string
 ? label: string
 ? show-label: boolean
 ? switchable: boolean
 ? resettable: boolean
 ? reset-text: string
 ? no-results-text: string
 */

export function Wrapper(element: HTMLDivElement) {
  const loading = element.getAttribute("loading") ?? false;
  const loadingText = element.getAttribute("loading-text");
  const label = element.getAttribute("label");
  const showLabel = element.hasAttribute("show-label") ?? false;
  const resettable = element.hasAttribute("resettable") ?? false;
  const resetText = element.getAttribute("reset-text") ?? "Reset";
  const noResultsText = element.getAttribute("no-results-text");

  const onReset = () => {
    element.dispatchEvent(
      new CustomEvent("reset", {
        bubbles: true,
      })
    );
  };

  const children = element.querySelectorAll("[data-element-type]");

  element.innerHTML = ``;

  if (loading) {
    const loaderElement = document.createElement("span");
    loaderElement.classList.add("loader");
    if (loadingText) loaderElement.innerHTML = loadingText ?? "";
    element.appendChild(loaderElement);
  }

  if (showLabel && label) {
    const labelElement = document.createElement("span");
    labelElement.classList.add("label");
    labelElement.innerHTML = label;
    element.appendChild(labelElement);
  }

  if (Array.from(children).length === 0 && noResultsText) {
    const noResultsElement = document.createElement("span");
    noResultsElement.classList.add("no-results");
    noResultsElement.innerHTML = noResultsText;
    element.appendChild(noResultsElement);
  } else {
    const noResultsElement = element.querySelector(".no-results");
    if (noResultsElement) noResultsElement.remove();
    children.forEach((child) => {
      element.appendChild(child);
    });
  }

  if (resettable) {
    const resetButton = document.createElement("button");
    resetButton.classList.add("reset");
    resetButton.innerHTML = resetText;
    element.appendChild(resetButton);
  }

  element.querySelector("button.reset")?.addEventListener("click", onReset);
}
