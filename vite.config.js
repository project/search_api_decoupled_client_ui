import path from "path";

import { fileURLToPath } from "url";

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

export default {
  build: {
    modulePreload: false,
    rollupOptions: {
      // We should avoid imports to be added as chunks, so we need to
      // specify the entry points manually
      input: {
        // src/components/**/*.ts
        chip: path.resolve(__dirname, "src/components/chip/chip.ts"),
        input: path.resolve(__dirname, "src/components/input/input.ts"),
        pager: path.resolve(__dirname, "src/components/pager/pager.ts"),
        result: path.resolve(__dirname, "src/components/result/result.ts"),
        summary: path.resolve(__dirname, "src/components/summary/summary.ts"),
        wrapper: path.resolve(__dirname, "src/components/wrapper/wrapper.ts"),

        // Expose index at build for preview.
        index: path.resolve(__dirname, "index.html"),
      },
      output: {
        dir: "dist",
        entryFileNames: "components/[name].js",
        assetFileNames: "assets/[ext]/[name].[ext]",
        chunkFileNames: "injector.js",
      },
    },
  },
};
