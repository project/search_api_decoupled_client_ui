# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# @search-api-decoupled/ui

## not released yet

## [2.0.1] - 2024-4-23

### Fixed

- fix not parsed rendered html excerpt on result component

## [2.0.0] - 2024-2-14

This version bump is needed due to the new client rendering method changing some component attributes and making them break on some cases.

## [1.1.0] - 2024-2-14

### Changed

- update components to the new client rendering method

### Fixed

- fix input initial-value

## [1.0.5] - 2023-10-11

### Changed

- improve chip on vertical mode
- improve reset button on wrapper component in vertical mode

## [1.0.4] - 2023-10-11

### Added

- add support for date in the result component

### Fixed

- fix category on the result component

## [1.0.3] - 2023-10-10

### Changed

- remove log
- improve responsiveness of the wrapper component
- adjust the reset button of the wrapper component

## [1.0.2] - 2023-10-06

### Changed

- Fix: references images at components was failing when were referenced since ne need to calculate the path on runtime.
- Minor: changed input identifier from "search_input-simple" to "search_input-default"

## [1.0.1] - 2023-10-05

### Changed

- small fix for the time taken in the summary component
- minor contribution fixes and improvements, including readme and changelog

## [1.0.0] - 2023-09-28

### Added

- Initial release including components:
  - chip
  - input
  - pager
  - result
  - summary
  - wrapper
